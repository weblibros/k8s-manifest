# Weblibro kubernetes manifest

This project stores the latest tag of the container image.  
It read by argo cd to update the application in development and production.  
[The Web libro project](https://gitlab.com/weblibros/app) triggers the pipeline to update the tag
